from model.card import Card
from model.deck_request import DeckRequest
import requests
from dotenv import dotenv_values
from pydantic import parse_obj_as
from typing import List


class CardsService:
    @staticmethod
    def getCardsInDeck(deck_id: str, nbCards: int) -> List[Card]:
        """Fetch api and get nbCards in the deck deck_id

        Args:
            deck_id (str): the deck id
            nbCards (int): nb card to retrieve

        Returns:
            List[Card]: the list of cards
        """
        config = dotenv_values(".env")
        api_host = config["API_HOST"]
        url = api_host + "/cartes/" + str(nbCards)
        reponse = requests.post(url,
                                json=DeckRequest(deck_id=deck_id).__dict__)
        cards = []
        for card in reponse.json():
            cards.append(Card(**card))
        return cards

    @staticmethod
    def countCardColor(listCard: List[Card]) -> dict:
        """Generate a dict which count how many cards of each color we have in the list

        Args:
            listCard (List[Card]): A list of cards

        Returns:
            dict:
        """
        result = {"C": 0, "S": 0, "H": 0, "D": 0}
        for card in listCard:
            if card.suit == "CLUBS":
                result["C"] += 1
            elif card.suit == "SPADES":
                result["S"] += 1
            elif card.suit == "HEARTS":
                result["H"] += 1
            elif card.suit == "DIAMONDS":
                result["D"] += 1
        return result