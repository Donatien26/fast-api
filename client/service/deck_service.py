from model.cards import Card
from model.deck_request import DeckRequest
import requests
from dotenv import dotenv_values


class DeckService:
    @staticmethod
    def getDeck():
        """Fetch api to get a deck id

        Returns:
            [type]: a string with the deck_id
        """
        config = dotenv_values(".env")
        api_host = config["API_HOST"]
        url = api_host + "/creer-un-deck/"
        reponse = requests.get(url)
        return reponse.json()