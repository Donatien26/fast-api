from PyInquirer import Separator, prompt
from client.view.abstract_view import AbstractView
from client.service.deck_service import DeckService


class DeckView(AbstractView):
    def __init__(self):
        self.questions = [{
            'type': 'list',
            'name': "choix",
            'message': "",
            'choices': [Separator(), "Retour au menu"]
        }]

    def display_info(self):
        try:
            print("Votre numéro de deck est le " + DeckService.getDeck())
        except:
            print("impossible de récuperer un deck réessayer plus tard")

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["choix"] == "Retour au menu":
            from client.view.start_view import StartView
            return StartView()