from PyInquirer import Separator, prompt
from client.view.abstract_view import AbstractView
from client.service.cards_service import CardsService


class CardDisplay(AbstractView):
    def __init__(self, nbcard: int, deck_id: str):
        self.questions = [{
            'type': 'list',
            'name': "choix",
            'message': "Voici vos cards",
            'choices': [Separator(), "Retour au menu"]
        }]
        self.nbcard = nbcard
        self.deck_id = deck_id

    def display_info(self):
        try:
            cards = CardsService.getCardsInDeck(deck_id=self.deck_id,
                                                nbCards=self.nbcard)
            print("Votre numéro de deck est le " + self.deck_id)
            print("Il est composé de " + str(self.nbcard) + "cards")
            main = []
            for card in cards:
                main.append(card.value + " " + card.suit)
            print("Composition de votre main :" + str(main))
            print("Composition de votre deck (en couleur)")
            print(CardsService.countCardColor(cards))
        except:
            print(
                "Impossible de récuperer une main depuis ce deck veuillez rééssayer"
            )

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["choix"] == "Retour au menu":
            from client.view.start_view import StartView
            return StartView()