from PyInquirer import Separator, prompt
from client.view.abstract_view import AbstractView


class StartView(AbstractView):
    def __init__(self):
        self.intro = [{
            'type':
            'list',
            'name':
            "intro",
            'message':
            "",
            'choices': [
                "Afficher un autre cocktail au hasard",
                Separator(),
                "Entrer dans l'application",
            ]
        }]

        self.questions = [{
            'type':
            'list',
            'name':
            "home",
            'message':
            "Choisissez l'action que vous voulez effectuer :",
            'choices': [
                "Récupérer un deck",
                Separator(),
                "Tirer des cartes dans un deck",
                Separator(),
                "Quitter l'application",
            ]
        }]

    def display_info(self):
        print("Bienvenu !!!!!")

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse["home"] == "Récupérer un deck":
            from client.view.deck_view import DeckView
            return DeckView()
        elif reponse["home"] == "Tirer des cartes dans un deck":
            from client.view.cards_view import CardsView
            return CardsView()
        else:
            pass
