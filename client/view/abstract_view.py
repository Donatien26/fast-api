from abc import ABC, abstractmethod


class AbstractView(ABC):
    @abstractmethod
    def display_info(self):
        """Generic method to display info to user
        """
        pass

    @abstractmethod
    def make_choice(self):
        """Generic method which prompt user's choice
        """
        pass
