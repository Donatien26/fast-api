from PyInquirer import Separator, prompt
from client.view.abstract_view import AbstractView
from client.service.cards_service import CardsService


class CardsView(AbstractView):
    def __init__(self):
        self.question_deck = [{
            'type': 'input',
            'name': "choix_deck",
            'message': "Veuillez saisir le numéro du deck.\n"
        }]
        self.question_nb_card = [{
            'type':
            'input',
            'name':
            "choix_nb_card",
            'message':
            "Veuillez saisir le nombre de cards a tirer.\n"
        }]

    def display_info(self):
        pass

    def make_choice(self):
        from client.view.cards_display_view import CardDisplay
        deck_number = prompt(self.question_deck)
        nb_card = prompt(self.question_nb_card)
        return CardDisplay(nbcard=int(nb_card["choix_nb_card"]),
                           deck_id=deck_number["choix_deck"])
