from client.view.start_view import StartView


class App():
    def run():
        current_vue = StartView()
        while current_vue:
            current_vue.display_info()
            current_vue = current_vue.make_choice()
