from unittest import TestCase,main
from client.service.cards_service import CardsService
from model.card import Card


class CardsServiceTest(TestCase):
    def test_countCardColor(self):
        json = [{
            "image": "https://deckofcardsapi.com/static/img/6C.png",
            "value": "6",
            "suit": "CLUBS",
            "code": "6C"
        }, {
            "image": "https://deckofcardsapi.com/static/img/4D.png",
            "value": "4",
            "suit": "DIAMONDS",
            "code": "4D"
        }]
        cards = []
        for card in json:
            cards.append(Card(**card))
        self.assertEqual({
            "C": 1,
            "S": 0,
            "H": 0,
            "D": 1
        }, CardsService.countCardColor(cards))


if __name__ == '__main__':
    main()