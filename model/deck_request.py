from pydantic import BaseModel


class DeckRequest(BaseModel):
    deck_id: str
