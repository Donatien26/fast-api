from pydantic import BaseModel
from model.card import Card
from typing import List


class Cards(BaseModel):
    success: bool
    cards: List[Card]
    deck_id: str
    remaining: int