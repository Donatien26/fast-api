from pydantic import BaseModel


class Card(BaseModel):
    image: str
    value: str
    suit: str
    code: str
