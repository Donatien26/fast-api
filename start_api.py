from webservice.main import app
import uvicorn
from dotenv import dotenv_values

if __name__ == '__main__':
    config = dotenv_values(".env")
    uvicorn.run(app, port=int(config["PORT"]), host=config["HOST"])