from fastapi import APIRouter
from webservice.service.cards_service import CardsService
from model.deck_request import DeckRequest

router = APIRouter()


@router.post("/cartes/{nombre_cartes}")
async def tirer_carte(nombre_cartes: int, deck: DeckRequest):
    cards = CardsService.getNbCardInDeck(deck.deck_id, nombre_cartes)
    return cards.cards