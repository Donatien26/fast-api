from model.cards import Cards
import requests


class CardsService:
    @staticmethod
    def getNbCardInDeck(deck_id: str, nb: int) -> Cards:
        url = "https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=" + str(
            nb)
        logger.debug("fetch " + url)
        reponse = requests.get(url)
        cards = Cards(**reponse.json())
        return cards