from model.deck import Deck
import requests


class DeckService:
    @staticmethod
    def getDeck(id: int) -> Deck:
        url = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
        reponse = requests.get(url)
        d = Deck(**reponse.json())
        return d
