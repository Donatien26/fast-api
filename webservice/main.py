from fastapi import FastAPI
from webservice.controller import deck_controller
from webservice.controller import cards_controller

app = FastAPI()

app.include_router(deck_controller.router)
app.include_router(cards_controller.router)