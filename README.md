Step 0 : this project is setup to work with pipenv which simplified the management of virtual env and dependencies.

`pip install pipenv`

Step 1 : Install dependencies

`pipenv install --python 3.8` (change with the python version installed on your computer)

Step 2: Start vitualenv

`pipenv shell`

Step 3: Start api

`python start_api.py`

Step 4: Start client

`python start_api.py`

enjoy :)

Step 5: Launch unittest

`python -m unittest discover test/`
